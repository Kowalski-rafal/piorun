let i = 0;
let fly;
let back;
const circle = document.querySelector(".circle");
const points = {
    top: [200, 264, 285, 345, 360, 460, 330, 315, 242, 228, 141, 200],
    left: [290, 530, 460, 675, 620, 980, 760, 815, 645, 698, 556, 290]
};

circle.style.top = points.top[0] + 'px';
circle.style.left = points.left[0] + 'px';

function circleGo() {
    circle.style.top = points.top[i + 1] + 'px';
    circle.style.left = points.left[i + 1] + 'px';
}

function circleBack() {
    circle.style.top = points.top[i - 1] + 'px';
    circle.style.left = points.left[i - 1] + 'px';
}

function mouseOver() {
    clearInterval(back);
    circleGo();
    i++;
    go = setInterval(() => {
        circleGo();
        i++;
    }, 1000);
}
function mouseOut() {
    clearInterval(fly);
    circleBack();
    i--;
    back = setInterval(() => {
        if (i != 0) i--;
        circleBack();
    }, 1000);
}

circle.addEventListener("mouseover", mouseOver);
circle.addEventListener("mouseout", mouseOut);


var x = null;
var y = null;

document.addEventListener('mousemove', onMouseUpdate, false);
document.addEventListener('mouseenter', onMouseUpdate, false);

function onMouseUpdate(e) {
    x = e.pageX;
    y = e.pageY;
    console.log('left ' + x, 'top ' + y);
}